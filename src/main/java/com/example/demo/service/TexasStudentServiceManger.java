package com.example.demo.service;

import com.example.demo.dao.StudentDao;
import com.example.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TexasStudentServiceManger implements StudentService {
    @Autowired
    private StudentDao studentDao;

    @Override
    public List<Student> findAll() {
        return (List<Student>)studentDao.findAll();
    }

    @Override
    public Student getById(long id) {
        return studentDao.findById(id).orElse(new Student());
    }

    @Override
    public Student update(Student student,long id) {
        Student student1 = studentDao.findById(id).orElse(new Student());
        student.setId(student1.getId());
        return studentDao.save(student);
    }

    @Override
    public Student add(Student student) {
       return studentDao.save(student);

    }

    @Override
    public boolean delete(long id) {
         studentDao.deleteById(id);
         return true;
    }

    @Override
    public List<Student> getAllStudnetByName(String name) {
      //  return studentDao.findByName(name);
        return null;
    }
}