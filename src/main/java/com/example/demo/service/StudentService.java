package com.example.demo.service;

import com.example.demo.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> findAll();

    Student getById(long Id);

    Student update(Student student,long id);

    Student add(Student student);

    boolean delete(long id);

    List<Student> getAllStudnetByName(String name);

 }
