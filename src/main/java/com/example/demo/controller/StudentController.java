package com.example.demo.controller;

import com.example.demo.model.Student;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

 @Controller
public class StudentController {

   @Autowired
   private StudentService studentService;

    @RequestMapping("/")
    //@ResponseBody
    public String home(){
        return "home.jsp";
    }

    @RequestMapping("/addStudent")
    public String addStudent(Student student){
       studentService.add(student);
       return "home";
    }

    @RequestMapping("/")
    public String getForm(){
        return "home";
    }

    @RequestMapping("/student/{id}")
    @ResponseBody
    public String getStudent(@PathVariable("id") long id){
        Student student= studentService.getById(id);
        return student.toString();
    }

    @RequestMapping("showStudent/{id}")
    public ModelAndView getAndShowStudnet(@PathVariable("id") long id){
          Student student= studentService.getById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("student");
        modelAndView.addObject("student",student);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/students", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public Student addNewStudent(@RequestBody Student student){
        return studentService.add(student);
    }

    @PutMapping(path = "student/{id}",consumes = {"application/json"}, produces = {"application/json"})
    @ResponseBody
    public Student updateStudent(@RequestBody Student student,@PathVariable("id") long id){
        return studentService.update(student,id);
    }

    @RequestMapping(path = "students/{id}",produces = {"application/json"})
    @ResponseBody
    public Student getStudnetInJson(@PathVariable("id") long id){
        Student student= studentService.getById(id);
        return student;
    }

    @RequestMapping(path="/students",produces = {"application/json"})
    @ResponseBody
    public List<Student> getAllStudents(){
        return studentService.findAll();
    }

    @DeleteMapping("student/{id}")
    @ResponseBody
    public boolean deleteStudent(@PathVariable("id") long id){
        return studentService.delete(id);
    }

    @PostMapping("/student")
    @ResponseBody
    public Student saveStudent(@RequestBody Student student){
        return studentService.add(student);
    }

    @PostMapping("studentByName")
     @ResponseBody
     public List<Student> getAllStudentsByName(@RequestBody Student student){
        return studentService.getAllStudnetByName(student.getName());
    }
}
