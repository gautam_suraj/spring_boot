package com.example.demo.dao;

import com.example.demo.model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/*
* spring data rest
*
*
*
*
* */

//@RepositoryRestResource(collectionResourceRel = "students", path="students")
public interface StudentDao  extends CrudRepository<Student, Long> {
    List<Student> findByName(String name);
    List<Student> findByMajorSubject(String majorSubject);
    List<Student> findByIdGreaterThan(long name);

   @Query("from Student where majorSubject=?1 order by name")
   List<Student> findByMajorSubjectSortedByName(String majorSubject);
}
